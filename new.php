<!DOCTYPE html>
<html>
<head>
  <meta charset="ISO-8859-1">
  <meta charset = "utf-16">
  <meta name = "viewport" content = "width = device-width, initial-scale = 1">

  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/classes.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">

  <title>LN Area | FairCoop</title>
</head>



<body>
<nav class="topbar navbar navbar-expand-md navbar-dark bg-dark">
  <div class="navbar-header">
		<a name="logo" class="navbar-brand top-logo" href="/">LNA</a>
	</div>

 	<div class="navbar-header">
    <a name="logo" class="navbar-brand header-logo" href="https://fair.coop" target="_blank"></a>
   </div>
  <div class="topbar_meta">
		<div>
			<button class="btn">En</button>
		</div>
			<div class="meta_social">
				<i class="social">
					<a href="https://t.me/joinchat/Gm6tEEiRjIw6VddA-1YMwA" target="_blank"><img src="img/telegram.png" height="25"></a>
				</i>
			</div>
		</div>
  </nav>

<nav class="menubar navbar navbar-expand-md navbar-dark bg-dark">
	<div class="container">
		<div class="navbar-collapse collapse" id="navbarCollapse">
      <ul class="nav navbar-nav navbar-right">
	    	<li style="color:yellow;">ADD NEW NODE</li>
     </ul>
		</div>
	</div>
</nav>

<?
// Global vars
$today = strtotime("now");
$date = date("Y-m-d");
?>

<div class="main"><br>
  <a name="info"></a><br><br><br>
  <h1 style="color:maroon;">Add a new Local Node to the network</h1>
  <br>

<?php
$servername = "localhost";
$username = "c30localfaircoop";
$password = "yya@KyDVD68";
$database = "c30localfaircoop";

// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Edit node
if ( $_POST['create'] ) {
// Fetch new data
		$id = "FCLN." . $_POST["id"];
		$n_ln = $_POST["name"];
		$n_assembly_notes = $_POST["assembly_notes"];
    $n_campaign = $_POST["campaign"];
    $n_chat = $_POST["chat"];
    $n_comments = $_POST["comments"];
    $n_contacted = $_POST["contacted"];
		$n_description = $_POST["description"];
		$n_establishing_assembly = $_POST["establishing_assembly"];
    $n_hours = $_POST["hours"];
    $n_income = $_POST["income"];
    $n_last_assembly = $_POST["last_assembly"];
    $n_last_ln_assembly = $_POST["last_ln_assembly"];
    $n_link = $_POST["link"];
    $n_lna_link = $_POST["lna_link"];
		$n_mail = $_POST["mail"];
    $n_methodology = $_POST["methodology"];
    $n_needs = $_POST["needs"];
    $n_new_wallets = $_POST["new_wallets"];
    $n_next_assembly = $_POST["next_assembly"];
		$n_participants = $_POST["participants"];
    $n_participants_n = $_POST["participants_n"];
		$n_projects = $_POST["projects"];
    $n_projects_n = $_POST["projects_n"];
		$n_ocp = $_POST["ocp"];
    $n_poe = $_POST["poe"];
    $n_region = $_POST["region"];
    $n_services = $_POST["services"];
    $n_tx = $_POST["tx"];
		$n_web = $_POST["web"];

// Calculate activity
	if ( $n_participants_n < 3 ) { $i_participants = -0.0000001;
	} else { $i_participants = 20;
	}
	$i_hours = 50;
	$i_poe = 10;
	$dif_last = ( $today - strtotime($n_last_assembly) ) / ( 24 * 60 * 60 );
	$dif_last_ln = ( $today - strtotime($n_last_ln_assembly) ) / ( 24 * 60 * 60 );
	if ( $dif_last > 90 ) { $i_last = 0.0000001; } else { $i_last = 50;
	}
	$i_last_ln = 200;
	$i_tx = 20;
	$i_projects = 1;
	$i_wallets = 4;

	$act = $n_participants_n / $i_participants + $n_hours / $i_hours + $n_poe / $i_poe + $n_tx / $i_tx + $n_projects_n / $i_projects + $n_wallets / $i_wallets - $dif_last / $i_last - $dif_last_ln / $i_last_ln;

	if ( $act < 0.5 ) { $activity = 0; } else { $activity = round($act,0);
	}

// Add node
	$sql = "INSERT INTO ln (id, name, description, email, web, OCP_project_profile, participants, establishing_assembly, assembly_notes, projects, region, activity, participants_n, hours, PoE, last_assembly, last_LN_assembly, next_assembly, tx, projects_n, new_wallets, link, LNA_link, campaign, needs, contacted, methodology, income, chat, comments, services, updated)
VALUES ('$id', '$n_ln', '$n_description', '$n_mail', '$n_web', '$n_ocp', '$n_participants', '$n_establishing_assembly', '$n_assembly_notes', '$n_projects', '$n_region', '$activity', '$n_participants_n', '$n_hours', '$n_poe', '$n_last_assembly', '$n_last_ln_assembly', '$n_next_assembly', '$n_tx', '$n_projects_n', '$n_new_wallets', '$n_link', '$n_lna_link', '$n_campaign', '$n_needs', '$n_contacted', '$n_methodology', '$n_income', '$n_chat', '$n_comments', '$n_services', '$date')";
//var_dump($sql); exit;

	if ($conn->query($sql) === TRUE) { echo "New record created successfully";
	} else { echo "Error: " . $sql . "<br>" . $conn->error;
	}

// Redirect to main page
	header("Location: index.php?ln=".$n_ln."#info"); exit;

} else {
?>
<hr>
<!-- Form -->
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	<div>
		<p>Node ID (3 CAPITAL LETTERS): <input type="text" name="id" maxlength="3">&nbsp;&nbsp;&nbsp;
		Node name: <input type="text" name="name">&nbsp;&nbsp;&nbsp;
		Region <select name="region">
				<option value="">select a region</option>
				<option value="africa">Africa</option>
				<option value="asia">Asia</option>
				<option value="europe">Europe</option>
				<option value="mediterranean">Mediterranean</option>
				<option value="middle-east">Middle East</option>
				<option value="north-america">North America</option>
				<option value="south-america">South America</option>
				<option value="other">other</option>
			</select></p>
		<p>
			<img src="img/web.png" height=35px; alt="web" />&nbsp;<input type="url" name="web">&nbsp;
			<img src="img/mail.png" height=40px; alt="mail" />&nbsp;<input type="email" name="mail">&nbsp;
			<img src="img/chat.png" height=40px; alt="chat" />&nbsp;<input type="text" name="chat">&nbsp;
			<img src="img/freedom.png" height=40px; alt="OCP" /><input type="text" name="ocp">
		</p>
	</div>
	<div class="ln" style="top:0;">
		<p>Description: <textarea name="description" cols="100"></textarea></p>
		<h4>People</h4>
		<p>Participants: <input type="number" name="participants_n">&nbsp;&nbsp;&nbsp;
		Usernames (without '@'): <input type="text" name="participants">&nbsp;&nbsp;&nbsp;
		Weekly hours of work (total): <input type="number" name="hours" value="<? echo $hours; ?>"></p>
		<p><strong>Link with the LN Area</strong> | Node side <input type="text" name="link">&nbsp;&nbsp;
		Area side <input type="text" name="lna_link">&nbsp;&nbsp;</p>
		<hr>
		<h4>Assemblies</h4>
		<p>First <input type="date" name="establishing_assembly">&nbsp;&nbsp;&nbsp;
		Last <input type="date" name="last_assembly">&nbsp;&nbsp;&nbsp;
		Next <input type="date" name="next_assembly">&nbsp;&nbsp;&nbsp;
		Last attendance to the LN assembly <input type="date" name="last_ln_assembly"></p>
		<p>Link to assembly notes: <input type="url" name="assembly_notes"></p>
		<hr>
		<h4>Economy</h4>
		<p>Projects: <input type="number" name="projects_n">&nbsp;&nbsp;&nbsp;
		Names: <input type="text" name="projects" size="60"></p>
		<p>Services: <input type="text" name="services" size="80"></p>
		<p>OCP transactions in the last month <input type="number" name="tx">&nbsp;&nbsp;
		New wallets last month <input type="number" name="new_wallets">&nbsp;&nbsp;
		PoE(s): <input type="number" name="poe"></p>
		<p>Main income: <input name="income" cols="80">&nbsp;&nbsp;
		Sustainability campaign: <input type="radio" name="campaign" value="0"> No <input type="radio" name="campaign" value="1"> Yes</p>
		<hr>
		<h3>Organization</h3>
		<p>Needs: <textarea name="needs" cols="80"></textarea><br>
		Work methodology: <textarea name="methodology" cols="80"></textarea><br>
		<hr>
		<p>Comments: <textarea name="comments"></textarea></p>
		<p><input type="submit" name="create" value="create"></p>
	</div>
</form>

<? }
$conn->close(); //Close connection
?>
</div>
</body>
</html>
