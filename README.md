



**BUGS**
* Screen width is not well adjusted.
* LN activity is not automatically updated, but only after editing the node.
* 'Assemblies' is a link to the assembly notes; it should be blue.
* The Telegram invite link is outdated.
* When several LNs are displayed at once it looks ugly.
* The activity blue bar doesn't show when several LNs are displayed at once.
