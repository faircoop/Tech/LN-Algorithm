<!DOCTYPE html>
<html>
<head>
  <meta charset="ISO-8859-1">
  <meta charset = "utf-16">
  <meta name = "viewport" content = "width = device-width, initial-scale = 1">

  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/classes.css">

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">

  <title>LN Area | FairCoop</title>
</head>



<body>
<nav class="topbar navbar navbar-dark bg-dark">
  <div class="navbar-header">
		<a name="logo" class="navbar-brand top-logo" href="index.php">LNA</a>
	</div>

 	<div class="navbar-header">
    <a name="logo" class="navbar-brand header-logo" href="https://fair.coop" target="_blank"></a>
   </div>
  <div class="topbar_meta">
		<div>
			<button class="btn">En</button>
		</div>
			<div class="meta_social">
				<i class="social">
					<a href="https://t.me/joinchat/Gm6tEEiRjIw6VddA-1YMwA" target="_blank"><img src="img/telegram.png" height="25"></a>
				</i>
			</div>
		</div>
  </nav>

<nav class="menubar navbar navbar-dark bg-dark">
	<div class="container">
		<div class="navbar-collapse collapse" id="navbarCollapse">
      <ul class="nav navbar-right">
	    	<li class=""><a href="#info">LNs</a></li>
	      <li class=""><a href="#contact">Area</a></li>
	      <li class=""><a href="https://wiki.fair.coop/en:local_nodes_area:start" target="_blank">Wiki</a></li>
     </ul>
		</div>
	</div>
</nav>

<div class="image-title">
	<img src="img/logo.png" height="80px" style="position:relative; top:-30px;">
	<h1>Local Nodes Area</h1>	
		<p class="title">The aim of this area is to have a clear and participative plan to improve the Local nodes work, needs, and feedback with the global.</p>
</div>

<div class="main"><br>
  <a name="info"></a><br><br>
  <h1>Local Nodes</h1>
  <br>

<?php
// Global vars
$today = strtotime("now");

// Connect to database
$servername = "localhost";
$username = "c30localfaircoop";
$password = "yya@KyDVD68";
$database = "c30localfaircoop";
$conn = new mysqli($servername, $username, $password, $database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// select LN
if ( $_GET['ln'] != "" ) { $ln = $_GET['ln'];
}
if ($_SERVER["REQUEST_METHOD"] == "POST") { $ln = $_POST["lnname"]; $camp = $_POST["campaign"];
}
?>
	<div class="selector">
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>#info" method="POST">
			<table><tr><td rowspan="2" style="background-color:white;padding:5px;margin:0px;">
					  <select name="lnname">
		  		  <option value="">Select LN</option>;
<?
$sql = "SELECT * FROM ln ORDER BY name ASC";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<option value=\"" . $row["name"]. "\">" . $row["name"]. "</option>";
    }
    echo "<option value=\"all\">ALL</option>";
		echo "</select>";
} else {
    echo "0 results";
}
?></td>
		 			<td rowspan="2">&nbsp;</td>
					<td rowspan="2" style="background-color:white;padding:5px;margin:0px;"><img src="img/campaign.png" height="50"/></td>
					<td style="background-color:white;padding:5px;margin:0px;"><input type="radio" name="campaign" value="yes">yes</input></td>
 					<td rowspan="2">&nbsp;</td>
 					<td rowspan="2" style="background-color:white;padding:5px;margin:0px;"><input type="submit" value="View"></td></tr>
				<tr><td style="background-color:white;padding:5px;margin:0px;"><input type="radio" name="campaign" value="no">no</input></td></tr>
			</table>
		</form>
<?
// Edit button
$sql = "SELECT * FROM ln where name = '$ln'";
$result = $conn->query($sql);
  while ($row = $result->fetch_assoc()) { $id = $row["id"];
	}
if ( $ln != "" && $ln != "all" ) { ?>
		<div class="edit">
			<form action="edit.php" method="POST">
<!--		User: <input name="u" />
		Password: <input name="p" /> -->
				<input type="hidden" name="id" value="<? echo $id; ?>" />
				<input type="hidden" name="ln" value="<? echo $ln; ?>" />
				<input type="submit" name="edit" value="Edit">
			</form>
		</div>
<? } else { ?>
<!-- Add new node -->
		<div class="new">
			<a href="new.php"><img src="img/new.png" alt="new node" height="50" /></a>&nbsp;&nbsp;&nbsp;
<!-- Download button -->
			<a href="export.php"><img src="img/download.png" alt="download database" height="50" /></a>
		</div>
<? } ?>
	</div>

<!--Fetch results-->
<?
if ( $camp == "yes" ) { $sql = "SELECT * FROM ln WHERE campaign='1'";
} elseif ( $camp == "no" ) { $sql = "SELECT * FROM ln WHERE campaign='0'";
} else { if ( $ln == "all" ) { $sql = "SELECT * FROM ln";
	} else { $sql = "SELECT * FROM ln WHERE name='$ln'";
	}
}
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
  while ($row = mysqli_fetch_assoc($result)) {
		$id = $row["id"];
		$activity = $row["activity"];
		$assembly_notes = $row["assembly_notes"];
		$campaign = $row["campaign"];
		$chat = $row["chat"];
		$comments = $row["comments"];
		$description = $row["description"];
		$establishing_assembly = $row["establishing_assembly"];
		$fairspot = $row["fairspot"];
		$income = $row["income"];
		$last_assembly = $row["last_assembly"];
		$link = $row["link"];
		$lna_link = $row["LNA_link"];
		$name = $row["name"];
		$needs = $row["needs"];
		$next_assembly = $row["next_assembly"];
		$mail = $row["email"];
		$methodology = $row["methodology"];
		$needs = $row["needs"];
		$ocp = $row["OCP_project_profile"];
		$participants = $row["participants"];
		$participants_n = $row["participants_n"];
		$poe = $row["PoE"];
		$projects = $row["projects"];
    $region = $row["region"];
    $services = $row["services"];
		$subnode = $row["issubnode"];
    $updated = $row["updated"];
		$web = $row["web"];
		$x = $row["x"];
		$y = $row["y"];

		$icon = "img/" . $id . ".png";
//var_dump($id); exit;
		?>
<!-- Show results -->
			<div class="icon"><img src="<? echo $icon; ?>" height="100" alt="no icon" /></div>
			<div class="contact">
				<a href="<? echo $web; ?>" target="_blank"><img src="img/web.png" height=35px; alt="web" /></a>&nbsp;
				<a href="mailto:<? echo $mail; ?>"><img src="img/mail.png" height=40px; alt="mail" /></a>&nbsp;
				<a href="<? echo $chat; ?>" target="_blank"><img src="img/chat.png" height=40px; alt="chat" /></a>&nbsp;
				<a href="<? echo $ocp; ?>" target="_blank"><img src="img/freedom.png" height=40px; alt="OCP" /></a>
				<br>
				<? $dif_updated = ( $today - strtotime($updated) ) / ( 24 * 60 * 60 );
				if ( $dif_updated > 180 ) { echo "<h2 style=\"color:maroon;\">OUTDATED</h2>"; }
				?>
			</div>
			<div class="ln_name"><h2><? echo $name;
			if ( $subnode ) { echo " (" . $subnode . ")"; }
			?></h2></div>
			<div class="activity"><? if ( $result->num_rows == 1 ) { include_once('activity.php');
				} else { echo "<strong>Activity = ".$activity . "</strong>"; } ?></div>
			<div class="description"><? echo $description; ?></div>
			<div class="ln <? echo $region; ?>">
				<div class="link"><img src="img/ln_logo.png" height="30" />&nbsp;<? echo $link; ?><br>
					<img src="img/header-logo.png" height="30" />&nbsp;<? echo $lna_link; ?></div>
				<p><a href="https://t.me/<? echo $link; ?>" target="_blank"><img src="img/user.png" height="30" /></a>&nbsp;&nbsp;
				<span <? if ( $participants_n < 3 ) { echo "class=\"warning\""; } ?>><? echo $participants; ?></span></p>
				<a href="<? echo $assembly_notes; ?>" target="_blank"><p><strong>Assemblies</strong></a>&nbsp;&nbsp;&nbsp;
				First: <? echo $establishing_assembly; ?>&nbsp;&nbsp;&nbsp;
				Last: <span <? $dif_last = ( $today - strtotime($last_assembly) ) / ( 24 * 60 * 60 ); if ( $dif_last > 90 ) { echo "class=\"warning\""; } ?>><? echo $last_assembly; ?></span>&nbsp;&nbsp;&nbsp;
				Next: <? echo $next_assembly; ?></p>
				<? if ( $services ) { ?><p><strong>Services</strong>: <? echo $services; ?></p><? } ?>
				<? if ( $methodology ) { ?><p><strong>Methodology</strong>: <? echo $methodology; ?></p><? } ?>
				<p><strong>Needs</strong>: <? echo $needs; ?></p>
				<div class="link"><? if ( $poe ) { echo "<img src=\"img/poe.png\" height=35 />"; } ?>&nbsp;&nbsp;&nbsp;
				<? if ( $fairspot ) { echo "<img src=\"img/fairspot.png\" height=35 />"; } ?>&nbsp;&nbsp;&nbsp;
				<? if ( $campaign ) { echo "<img src=\"img/campaign.png\" height=35 />"; } ?></div>
				<h4>Economy</h4>
					<p><? if ( $income ) { echo "<strong>Income</strong>: " . $income; } ?>&nbsp;&nbsp;&nbsp;
					<? if ( $projects ) { echo "<strong>Productive projects</strong>: " . $projects; } ?>&nbsp;&nbsp;&nbsp;</p>
				<p><? if ( $comments ) { echo "<br>" . $comments; } ?></p>
				<div class="link"><a href="http://map.fairplayground.info/map-localnodes/" target="_blank"><img src="img/local.png" height="30" /></a>&nbsp;<? echo "<span style=\"color:grey;\">" . $y . ", " . $x . "</span>"; ?>
				</div>
			</div>
			<div style="float: left; color: grey; position: relative; top: -132px; padding: 10px;"><h4><? echo $id; ?></h4></div>
<?
  }
} else { echo "No node selected.";
} ?>

<? $conn->close(); //Close connection ?>

  <a name="contact"></a><br><br><br><br><br><br>
  <hr>
  <h1>LN Area</h1>
Telegram group: <a href="https://t.me/joinchat/Gm6tEEiRjIw6VddA-1YMwA" target="_blank">LN Area</a><br>
Email: <strong>localnodes@fair.coop</strong><br>
<br>
Pad: <a href="https://board.net/p/LNA" target="_blank">LNA</a>
<br><br><br><br><br><br>
<p class="center"><a href="https://fair.coop" target="_blank">FairCoop 2018</a></p>
  <br><br><br><br><br><br>
  <br>
</div>
</body>
</html>
