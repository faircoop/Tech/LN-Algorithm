<!DOCTYPE html>
<html>
<head>
  <meta charset="ISO-8859-1">
  <meta charset = "utf-16">
  <meta name = "viewport" content = "width = device-width, initial-scale = 1">

  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/classes.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">

  <title>LN Area | FairCoop</title>
</head>



<body>
<nav class="topbar navbar navbar-expand-md navbar-dark bg-dark">
  <div class="navbar-header">
		<a name="logo" class="navbar-brand top-logo" href="/">LNA</a>
	</div>

 	<div class="navbar-header">
    <a name="logo" class="navbar-brand header-logo" href="https://fair.coop" target="_blank"></a>
   </div>
  <div class="topbar_meta">
		<div>
			<button class="btn">En</button>
		</div>
			<div class="meta_social">
				<i class="social">
					<a href="https://t.me/joinchat/Gm6tEEiRjIw6VddA-1YMwA" target="_blank"><img src="img/telegram.png" height="25"></a>
				</i>
			</div>
		</div>
  </nav>

<nav class="menubar navbar navbar-expand-md navbar-dark bg-dark">
	<div class="container">
		<div class="navbar-collapse collapse" id="navbarCollapse">
      <ul class="nav navbar-nav navbar-right">
	    	<li style="color:yellow;"><a href="#info">EDIT</a></li>
     </ul>
		</div>
	</div>
</nav>

<?
// Global vars
$today = strtotime("now");
$date = date("Y-m-d");

// Select node
$id = $_POST['id'];
//$u = $_POST['u'];
//$p = $_POST['p'];
$ln = $_POST['ln'];
?>
<div class="main"><br>
  <a name="info"></a><br><br><br>
  <h1 style="color:maroon;">Edit <? echo $ln; ?> Local Node</h1>
  <br>

<?php
$servername = "localhost";
$username = "c30localfaircoop";
$password = "yya@KyDVD68";
$database = "c30localfaircoop";

// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Edit node
if ( $_POST['update'] ) {
// Fetch new data
		$n_ln = $_POST["name"];
		$n_assembly_notes = $_POST["assembly_notes"];
    if ( $_POST["campaign"] == 1 ) { $n_campaign = $_POST["campaign"]; }
    $n_chat = $_POST["chat"];
    $n_comments = $_POST["comments"];
    $n_contacted = $_POST["contacted"];
		$n_description = $_POST["description"];
		$n_establishing_assembly = $_POST["establishing_assembly"];
    if ( $_POST["fairspot"] == 1 ) { $n_fairspot = $_POST["fairspot"]; }
    $n_hours = $_POST["hours"];
    $n_income = $_POST["income"];
    $n_last_assembly = $_POST["last_assembly"];
    $n_last_ln_assembly = $_POST["last_ln_assembly"];
    $n_link = $_POST["link"];
    $n_lna_link = $_POST["lna_link"];
		$n_mail = $_POST["mail"];
    $n_methodology = $_POST["methodology"];
    $n_needs = $_POST["needs"];
    $n_new_wallets = $_POST["new_wallets"];
    $n_next_assembly = $_POST["next_assembly"];
		$n_ocp = $_POST["ocp"];
		$n_participants = $_POST["participants"];
    $n_participants_n = $_POST["participants_n"];
		$n_projects = $_POST["projects"];
    $n_projects_n = $_POST["projects_n"];
    $n_poe = $_POST["poe"];
    $n_region = $_POST["region"];
    $n_services = $_POST["services"];
    $n_subnode = $_POST["subnode"];
    $n_tx = $_POST["tx"];
		$n_web = $_POST["web"];
    $n_x = $_POST["x"];
    $n_y = $_POST["y"];

// Calculate activity
//// Nr. participants
	if ( $n_participants_n < 3 ) { $i_participants = -0.0000001;
	} else { $i_participants = 15;
	}
//// Weekly hours
	$i_hours = 20;
//// PoE
	$i_poe = 10;
//// Time since last open assembly
	$dif_last = ( $today - strtotime($n_last_assembly) ) / ( 24 * 60 * 60 );
	$dif_last_ln = ( $today - strtotime($n_last_ln_assembly) ) / ( 24 * 60 * 60 );
	if ( $dif_last > 90 ) { $i_last = 0.0000001; } else { $i_last = 50;
	}
//// Time since last attendance to common LN assembly
	$i_last_ln = 200;
//// Number of transactions
	$i_tx = 20;
//// Number of projects
	$i_projects = 1;
//// Number of new wallets in the last month
	$i_wallets = 4;
//// Activity
	$act = $n_participants_n / $i_participants + $n_hours / $i_hours + $n_poe / $i_poe + $n_tx / $i_tx + $n_projects_n / $i_projects + $n_wallets / $i_wallets - $dif_last / $i_last - $dif_last_ln / $i_last_ln;
//// Round to 0 decimals
	if ( $act < 0.5 ) { $activity = 0;
	} else { $activity = round($act,0);
	}
// Update table
	$sql = "UPDATE ln SET name='$n_ln' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET activity='$activity' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET assembly_notes='$n_assembly_notes' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	if ( $n_campaign == 1 ) {
		$sql = "UPDATE ln SET campaign='$n_campaign' WHERE id='$id'";
		if ($conn->query($sql) === TRUE) {
	    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
		}
	}
	$sql = "UPDATE ln SET description='$n_description' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET email='$n_mail' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	if ( $n_fairspot != "" ) {
		$sql = "UPDATE ln SET fairspot='$n_fairspot' WHERE id='$id'";
		if ($conn->query($sql) === TRUE) {
	    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
		}
	}
	$sql = "UPDATE ln SET hours='$n_hours' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET last_assembly='$n_last_assembly' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET last_LN_assembly='$n_last_ln_assembly' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET methodology='$n_methodology' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET new_wallets='$n_new_wallets' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET OCP_project_profile='$n_ocp' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET participants='$n_participants' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET participants_n='$n_participants_n' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	if ( $n_poe != "" ) {
		$sql = "UPDATE ln SET PoE='$n_poe' WHERE id='$id'";
		if ($conn->query($sql) === TRUE) {
  	  echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
		}
	}
	$sql = "UPDATE ln SET projects='$n_projects' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	if ( $n_region != "" ) {
		$sql = "UPDATE ln SET region='$n_region' WHERE id='$id'";
		if ($conn->query($sql) === TRUE) {
	    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
		}
	}
	if ( $n_subnode != "0" ) {
		$sql = "UPDATE ln SET issubnode='$n_subnode' WHERE id='$id'";
		if ($conn->query($sql) === TRUE) {
  	  echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
		}
	}
	$sql = "UPDATE ln SET updated='$date' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET web='$n_web' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET x='$n_x' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET y='$n_y' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}

	$sql = "UPDATE ln SET establishing_assembly='$n_establishing_assembly' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET next_assembly='$n_next_assembly' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET tx='$n_tx' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET projects_n='$n_projects_n' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET link='$n_link' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET LNA_link='$n_lna_link' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET needs='$n_needs' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET contacted='$n_contacted' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET income='$n_income' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET chat='$n_chat' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET services='$n_services' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}
	$sql = "UPDATE ln SET comments='$n_comments' WHERE id='$id'";
	if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully"; } else { echo "Error updating record: " . $conn->error;
	}

// Redirect to main page
	header("Location: index.php?ln=".$n_ln."#info"); exit;

} else {
// Fetch previous info
	$sql = "SELECT * FROM ln where name = '$ln'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
// output data of each row
		while($row = $result->fetch_assoc()) {
			$ln = $row["name"];
			$assembly_notes = $row["assembly_notes"];
      $campaign = $row["campaign"];
      $chat = $row["chat"];
			$comments = $row["comments"];
      $contacted = $row["contacted"];
			$description = $row["description"];
			$establishing_assembly = $row["establishing_assembly"];
      $fairspot = $row["fairspot"];
      $hours = $row["hours"];
      $income = $row["income"];
      $last_assembly = $row["last_assembly"];
      $last_ln_assembly = $row["last_LN_assembly"];
      $link = $row["link"];
      $lna_link = $row["LNA_link"];
			$mail = $row["email"];
      $methodology = $row["methodology"];
      $needs = $row["needs"];
      $new_wallets = $row["new_wallets"];
      $next_assembly = $row["next_assembly"];
			$ocp = $row["OCP_project_profile"];
      $poe = $row["PoE"];
			$participants = $row["participants"];
      $participants_n = $row["participants_n"];
			$projects = $row["projects"];
      $projects_n = $row["projects_n"];
      $region = $row["region"];
	    $services = $row["services"];
	    $subnode = $row["issubnode"];
      $tx = $row["tx"];
			$web = $row["web"];
      $x = $row["x"];
      $y = $row["y"];
		}
	}
}
?>
<hr>

<!-- Form -->
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	<div>
		<p>Node name <input type="text" name="name" value="<? echo $ln; ?>">&nbsp;&nbsp;&nbsp;
			Subnode of <select name="subnode">
    <option value="0">none</option>;
<?
$sql = "SELECT name FROM ln WHERE issubnode=0";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
	// output data of each row
  while($row = $result->fetch_assoc()) {
		echo "<option value=\"" . $row["name"]. "\">" . $row["name"]. "</option>";
  }
	echo "</select>";
}
?>
		</p>
		<!-- contact details -->
		<p>
			<img src="img/web.png" height=40px; alt="web" />&nbsp;<input type="url" name="web" value="<? echo $web; ?>">&nbsp;
			<img src="img/mail.png" height=40px; alt="mail" />&nbsp;<input type="email" name="mail" value="<? echo $mail; ?>">&nbsp;
			<img src="img/chat.png" height=40px; alt="chat" />&nbsp;<input type="text" name="chat" value="<? echo $chat; ?>">&nbsp;
			<img src="img/freedom.png" height=40px; alt="OCP" /><input type="text" name="ocp" value="<? echo $ocp; ?>">
		</p>
		<!-- localization -->
		<p><img src="img/local.png" height=40px; alt="Localization" />&nbsp;&nbsp;&nbsp;
			Long.&nbsp;&#8596;&nbsp;<input type="text" name="x" value="<? echo $x; ?>">&nbsp;&nbsp;
			Lat.&nbsp;&#8597;&nbsp;<input type="text" name="y" value="<? echo $y; ?>">&nbsp;&nbsp;
			Region <select name="region">
				<option value="">select a region</option>
				<option value="africa">Africa</option>
				<option value="asia">Asia</option>
				<option value="europe">Europe</option>
				<option value="mediterranean">Mediterranean</option>
				<option value="middle-east">Middle East</option>
				<option value="north-america">North America</option>
				<option value="south-america">South America</option>
				<option value="other">other</option>
			</select></p>
	</div>
	<!-- main box -->
	<div class="ln <? echo $region; ?>" style="top:0;">
		<p>Description: <textarea name="description" cols="100"><? echo $description; ?></textarea></p>
		<h4>People</h4>
		<p>Participants: <input type="number" name="participants_n" value="<? echo $participants_n; ?>">&nbsp;&nbsp;&nbsp;Usernames: <input type="text" name="participants" value="<? echo $participants; ?>"> Weekly hours of work (total): <input type="number" name="hours" value="<? echo $hours; ?>"></p>
		<p><strong>Link with the LN Area</strong> | Node side <input type="text" name="link" value="<? echo $link; ?>">&nbsp;&nbsp;Area side <input type="text" name="lna_link" value="<? echo $lna_link; ?>">&nbsp;&nbsp;Last contacted: <input type="date" name="contacted" value="<? echo $contacted; ?>"></p>
		<hr>
		<h4>Assemblies</h4>
		<p>First <input type="date" name="establishing_assembly" value="<? echo $establishing_assembly; ?>"> Last <input type="date" name="last_assembly" value="<? echo $last_assembly; ?>"> Next <input type="date" name="next_assembly" value="<? echo $next_assembly; ?>"> Last attendance to the LN assembly <input type="date" name="last_ln_assembly" value="<? echo $last_ln_assembly; ?>"></p>
		<p>Link to assembly notes: <input type="url" name="assembly_notes" value="<? echo $assembly_notes; ?>"></p>
		<hr>
		<h4>Economy</h4>
		<p>Productive projects: <input type="number" name="projects_n" value="<? echo $projects_n; ?>">&nbsp;&nbsp;&nbsp;Short description: <input type="text" name="projects" size="60" value="<? echo $projects; ?>"></p>
		<p>Services: <input type="text" name="services" size="80" value="<? echo $services; ?>"></p>
		<p>FairCoin transactions in the last month <input type="number" name="tx" value="<? echo $tx; ?>">&nbsp;&nbsp;
		New wallets last month <input type="number" name="new_wallets" value="<? echo $new_wallets; ?>">&nbsp;&nbsp;
		PoE(s): <input type="number" name="poe" value="<? echo $poe; ?>"></p>
		<p>Main income: <input name="income" cols="80" value="<? echo $income; ?>">&nbsp;&nbsp;
		Sustainability campaign: <input type="radio" name="campaign" value="0"> No <input type="radio" name="campaign" value="1"> Yes</p>
		<hr>
		<h3>Organization</h3>
		<p>Needs: <textarea name="needs" cols="80"><? echo $needs; ?></textarea></p>
		<p>Work methodology: <textarea name="methodology" cols="100"><? echo $methodology; ?></textarea>
		<p>Fairspot: <input type="radio" name="fairspot" value="0"> No <input type="radio" name="fairspot" value="1"> Yes</p>
		<hr>
		<p>Comments: <textarea name="comments" cols="100"><? echo $comments; ?></textarea></p>
		<input type="hidden" name="id" value="<? echo $id; ?>">
		<p><input type="submit" name="update" value="update"></p>
	</div>
</form>

<? $conn->close(); //Close connection ?>
</div>
</body>
</html>
