<?
// Global vars
$today = strtotime("now");
$date = date("Y-m-d");

// Connect to database
$servername = "localhost";
$username = "c30localfaircoop";
$password = "yya@KyDVD68";
$database = "c30localfaircoop";
$conn = new mysqli($servername, $username, $password, $database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Select data
$sql = "SELECT * FROM ln";
$result = mysqli_query($conn, $sql);

echo "<br><br>";
echo "<h2>Download LN data</h2>";

// Select format
switch ($_GET['f']) {
	// JSON
	case "json":
		echo "<h3>LN data in JSON format</h3>";
		$json_array = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$json_array[] = $row;
			echo '<pre>';
			print_r($json_array);
			echo '</pre>';
		}
		echo json_encode($json_array);
		break;
	// screen
	case "screen":
		echo "<h3>LN data in CSV format</h3>";
		echo "<table>";
		echo "<tr><td>ID</td><td>Name</td><td>Activity</td><td>Assembly notes</td><td>Campaign</td>";
		echo "<td>Chat</td><td>Comments</td><td>Contacted</td><td>Description</td><td>Email</td>";
		echo "<td>Establishing assembly</td><td>Fairspot</td><td>Hours</td><td>Income</td><td>Subnode of</td>";
		echo "<td>Last assembly</td><td>Last LN assembly</td><td>Link</td><td>LNA link</td><td>Methodology</td>";
		echo "<td>Needs</td><td>New wallets</td><td>Next assembly</td><td>OCP profile</td><td>Participants</td>";
		echo "<td>#</td><td>PoE</td><td>Projects</td><td>#</td><td>Region</td>";
		echo "<td>Services</td><td>Tx</td><td>Updated</td><td>Web</td><td>x</td>";
		echo "<td>y</td>";
		echo "</tr>";
		$query = "SELECT * FROM ln ORDER BY id ASC";
		$result = mysqli_query($conn, $query);
		while ( $row = mysqli_fetch_array($result) ) {
			echo "<tr><td>" . $row['id'] . "</td>";
			echo "<td>" . $row['name'] . "</td>";
			echo "<td>" . $row['activity'] . "</td>";
			echo "<td>" . $row['assembly_notes'] . "</td>";
			echo "<td>" . $row['campaign'] . "</td>";
			echo "<td>" . $row['chat'] . "</td>";
			echo "<td>" . $row['comments'] . "</td>";
			echo "<td>" . $row['contacted'] . "</td>";
			echo "<td>" . $row['description'] . "</td>";
			echo "<td>" . $row['email'] . "</td>";
			echo "<td>" . $row['establishing_assembly'] . "</td>";
			echo "<td>" . $row['fairspot'] . "</td>";
			echo "<td>" . $row['hours'] . "</td>";
			echo "<td>" . $row['income'] . "</td>";
			echo "<td>" . $row['issubnode'] . "</td>";
			echo "<td>" . $row['last_assembly'] . "</td>";
			echo "<td>" . $row['last_LN_assembly'] . "</td>";
			echo "<td>" . $row['link'] . "</td>";
			echo "<td>" . $row['LNA_link'] . "</td>";
			echo "<td>" . $row['methodology'] . "</td>";
			echo "<td>" . $row['needs'] . "</td>";
			echo "<td>" . $row['new_wallets'] . "</td>";
			echo "<td>" . $row['next_assembly'] . "</td>";
			echo "<td>" . $row['OCP_project_profile'] . "</td>";
			echo "<td>" . $row['participants'] . "</td>";
			echo "<td>" . $row['participants_n'] . "</td>";
			echo "<td>" . $row['PoE'] . "</td>";
			echo "<td>" . $row['projects'] . "</td>";
			echo "<td>" . $row['projects_n'] . "</td>";
			echo "<td>" . $row['region'] . "</td>";
			echo "<td>" . $row['services'] . "</td>";
			echo "<td>" . $row['tx'] . "</td>";
			echo "<td>" . $row['updated'] . "</td>";
			echo "<td>" . $row['web'] . "</td>";
			echo "<td>" . $row['x'] . "</td>";
			echo "<td>" . $row['y'] . "</td>";
			echo "</tr>";
		}
		echo "</table>";
		break;
	// CSV
	case "csv":
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=ln.csv');
		$output = fopen("php://output", "w");
		fputcsv($output, array('ID', 'Name', 'Activity', 'Assembly notes', 'Campaign', 'Chat', 'Comments', 'Contacted', 'Description', 'Email', 'Establishing assembly', 'Fairspot', 'Hours', 'Income', 'Subnode of', 'Last assembly', 'Last LN assembly','Link', 'LNA link', 'Methodology', 'Needs', 'New wallets', 'Next assembly', 'OCP profile', 'Participants', '#', 'PoE', 'Projects', '#', 'Region', 'Services', 'Tx', 'Updated', 'Web', 'x', 'y'));
		$query = "SELECT * FROM ln ORDER BY id ASC";
		$result = mysqli_query($conn, $query);
		while ( $row = mysqli_fetch_assoc($result) ) {
			fputcsv($output, $row);
		}
		fclose($output);
		break;
	default:
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="ISO-8859-1">
  <meta charset = "utf-16">
  <meta name = "viewport" content = "width = device-width, initial-scale = 1">

  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/classes.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">

  <title>LN Area | FairCoop</title>
</head>



<body>
<nav class="navbar bg-dark" style="height:50px;">
	<div class="container">
		<div class="navbar-collapse collapse" id="navbarCollapse">
      <ul class="nav navbar-nav navbar-right">
	    	<li style="color:yellow;"><a href="index.php">Home</a></li>
     </ul>
		</div>
	</div>
</nav>

<div>

	<div>
		<p><a href="export.php?f=json"><img src="img/download.png" height="50" />&nbsp;JSON</a><br>
		<a href="export.php?f=csv"><img src="img/download.png" height="50" />&nbsp;CSV</a></p>
		<p><a href="export.php?f=screen"><img src="img/screen.png" height="50" />&nbsp;Print on screen</a></p>
	</div>
</div>
</body>
</html>
<?	
}
$conn->close(); //Close connection
?>

